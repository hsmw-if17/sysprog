#include <unistd.h> /* lstat(), readlink() */
#include <sys/stat.h> /* struct stat */

#include <linux/limits.h> /* PATH_MAX */

#include "mylib.h"

char* isSoftlink(char* filename)
{
    static char linkname[PATH_MAX+1]; /*+1 für nullbyte!*/

    struct stat eigenschaften;

    /* Eigenschaften abrufen. lstat() weil softlink!! */
    if( lstat(filename, &eigenschaften) < 0 )
    {
        /* errno von stat gesetzt, kann im Hauptprogramm abgerufen werden */
        return NULL;
    }

    /* wenn nicht lstat() dann wird dem link gefolgt und es wird niemals
     der Dateityp gleich S_IFLNK sein! */

    /* falls Softlink... */
    if( (eigenschaften.st_mode & S_IFMT) == S_IFLNK )
    {
        ssize_t bytes = readlink( filename, linkname, PATH_MAX );
        if( bytes < 0 )
        {
            /* errno gesetzt, kann im HP abgerufen werden */
            return NULL;
        }

        /* "readlink does NOT append a null byte" */
        linkname[bytes] = '\0'; /* nullbyte nach string */

        /* gibt char[] adresse zurück */
        return linkname;
    }
    /*sonst, NULL*/
    return NULL;
}
