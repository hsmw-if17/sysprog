#include <stdlib.h> /* standardfunktionen */
#include <unistd.h> /* dateifunktionen */
#include <fcntl.h>  /* mehr dateifunktionen */
#include <stdio.h>  /* standard ein-/ausgabe */
#include <sys/stat.h> /* struct stat - Dateiinformationen */

#include <dirent.h> /* struct dirent - Verzeichniseintrag */

#include <pwd.h> /* /etc/passwd - Nuzternamenabfrage */
#include <grp.h> /* /etc/group - Gruppennamenabfrage */

#include <time.h> /* Umwandeln von Unix Timestamp in String */

#define TIMEFORMAT "%d.%m.%Y %T" /* dd.mm.YYYY HH:MM:SS = 19 chars */
#define TIMESTRINGBUFFERSIZE 32  /* bisschen extra */

int timeToString(time_t time, char* buffer);
void printFileInformation( struct stat eigenschaften );

int main(int argc, char* argv[])
{
    /* Fehler bei falschen parametern */
    if(argc != 2)
    {
        printf("Nutzung:\n");
        printf("\t%s DATEINAME\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    struct stat eigenschaften;
    char* path = argv[1];

    /* dateieigenschaften abrufen */
    /* lstat, damit softlinks gefunden werden! */
    if( lstat(path, &eigenschaften) < 0 )
    {
        perror("Eigenschaften abrufen");
        exit(EXIT_FAILURE);
    }

    /* falls Datei ein Verzeichnis ist... */
    if( (eigenschaften.st_mode & S_IFMT) == S_IFDIR )
    {
        /* Verzeichnis öffnen */
        DIR* verzeichnis = opendir(path);
        if(verzeichnis == NULL)
        {
            perror("Verzeichnis oeffnen");
            exit(EXIT_FAILURE);
        }

        /* in das Verzeichnis wechseln (nachfolgend wichtig) */
        if( chdir(path) < 0 )
        {
            perror("Verzeichnis betreten");
            exit(EXIT_FAILURE);
        }

        struct dirent* eintrag = NULL;

        /* Verzeichniseinträge lesen... */
        while( (eintrag = readdir(verzeichnis)) != NULL)
        {
            /* Dateieigenschaften abrufen (wiederverwenden der var.) */
            if( lstat(eintrag->d_name, &eigenschaften) < 0 )
            {
                perror("Eigenschaften abrufen");
                exit(EXIT_FAILURE);
            }

            /*eintrag->d_name ist Dateiname OHNE PFAD
             deshalb wechseln in das Verzeichnis wichtig*/
            /* alternativ aus pfad+"/"+eintrag->d_name zusammensetzen */

            /* ...und für jede Datei Informationen ausgeben */
            printf("== %s ==\n", eintrag->d_name);
            printFileInformation( eigenschaften );
            printf("-----------------------------\n");
        }

        closedir(verzeichnis);
    }
    else
    {
        /* sonst nur einmalig informationen ausgeben */
        printFileInformation( eigenschaften );
    }

    /* fertig! */
    return EXIT_SUCCESS;
}

void printFileInformation( struct stat eigenschaften )
{
    printf("Gerätenummer: %ld\n", eigenschaften.st_dev);
    printf("Inode Nummer: %ld\n", (long)eigenschaften.st_ino);
    printf("Hardlinkanzahl: %ld\n", (long)eigenschaften.st_nlink);

    /*ST_MODE:
     * 00        0            000
     * dateityp, spezialbits, rechte*/

    /*Dateityp prüfen!*/
    printf("Dateityp: ");
    switch( eigenschaften.st_mode & S_IFMT )
    {
    case S_IFSOCK:
        printf("Socket");
        break;
    case S_IFLNK:
        printf("Softlink");
        break;
    case S_IFREG:
        printf("normale Datei");
        break;
    case S_IFBLK:
        printf("blockorientiertes Geraet");
        break;
    case S_IFDIR:
        printf("Verzeichnis");
        break;
    case S_IFCHR:
        printf("zeichenorientiertes Geraet");
        break;
    case S_IFIFO:
        printf("Pipe");
        break;
    }
    /*nur die ersten 2 ziffern*/
    printf(" (%o)\n", ((eigenschaften.st_mode>>12)) );

    /* nachfolgendes etwas umständlich, demonstriert aber viele flags! */

    /* Spezialbits */
    /*NUR die vierte Ziffer!*/
    printf("Spezialbits: %o\n", (eigenschaften.st_mode>>9)&07);
    if(eigenschaften.st_mode & S_ISUID) printf("\tSet UID Bit\n");
    if(eigenschaften.st_mode & S_ISGID) printf("\tSet GID Bit\n");
    if(eigenschaften.st_mode & S_ISVTX) printf("\tSticky Bit\n");

    /*Zugriffsrechte als Oktalziffern*/
    printf("Zugriffsrechte: %o\n", eigenschaften.st_mode&0777);

    printf("\tBesitzer:");
    if( (eigenschaften.st_mode & S_IRWXU) == S_IRWXU) printf(" Lesen, Schreiben, Ausfuehren\n");
    else {
        if(eigenschaften.st_mode & S_IRUSR) printf(" Lesen");
        if(eigenschaften.st_mode & S_IWUSR) printf(" Schreiben");
        if(eigenschaften.st_mode & S_IXUSR) printf(" Ausfuehren");
        printf("\n");
    }

    printf("\tGruppe:");
    if( (eigenschaften.st_mode & S_IRWXG) == S_IRWXG) printf(" Lesen, Schreiben, Ausfuehren\n");
    else {
        if(eigenschaften.st_mode & S_IRGRP) printf(" Lesen");
        if(eigenschaften.st_mode & S_IWGRP) printf(" Schreiben");
        if(eigenschaften.st_mode & S_IXGRP) printf(" Ausfuehren");
        printf("\n");
    }

    printf("\tAndere:");
    if( (eigenschaften.st_mode & S_IRWXO) == S_IRWXO) printf(" Lesen, Schreiben, Ausfuehren\n");
    else {
        if(eigenschaften.st_mode & S_IROTH) printf(" Lesen");
        if(eigenschaften.st_mode & S_IWOTH) printf(" Schreiben");
        if(eigenschaften.st_mode & S_IXOTH) printf(" Ausfuehren");
        printf("\n");
    }

    /* Benutzername */
    struct passwd* pwd;
    char* usrname;
    if( (pwd = getpwuid( eigenschaften.st_uid )) != NULL )
    {
        usrname = pwd->pw_name;
    }
    else
    {
        fprintf(stderr, "Nuzter existiert nicht!\n");
        usrname = "";
    }
    printf("Benutzer: %s (%ld)\n", usrname, (long)eigenschaften.st_uid);

    /* Gruppenname */
    struct group* grp;
    char* grpname;
    if( (grp = getgrgid( eigenschaften.st_gid )) != NULL )
    {
        grpname = grp->gr_name;
    }
    else
    {
        fprintf(stderr, "Gruppe existiert nicht!\n");
        grpname = "";
    }
    printf("Gruppe: %s (%ld)\n", grpname, (long)eigenschaften.st_gid);

    /* Dateigröße */
    printf("Dateigroesse: %ld Bytes\n", (long)eigenschaften.st_size);

    /* Zeiten */
    char timebuf[TIMESTRINGBUFFERSIZE];

    if( timeToString(eigenschaften.st_atime, timebuf) )
    {
        printf("Letzter Zugriff: %s (%ld)\n", timebuf, eigenschaften.st_atime);
    }
    if( timeToString(eigenschaften.st_mtime, timebuf) )
    {
        printf("Letzte Datei-Aenderung: %s (%ld)\n", timebuf, eigenschaften.st_mtime);
    }
    if( timeToString(eigenschaften.st_ctime, timebuf) )
    {
        printf("Letzte inode-Aenderung: %s (%ld)\n", timebuf, eigenschaften.st_ctime);
    }
}

int timeToString(time_t time, char* buffer)
{
    /* POSIX marks [ctime] obsolete and recommends strftime instead.
     * The C standard also recommends strftime instead of ctime and
     * ctime_s because strftime is more flexible and locale-sensitive.
     *
     * sonst einfach: char* str = ctime(&eigenschaften.st_mtime);
     */

    struct tm* timeLocal = localtime(&time);

    int bytesWritten = strftime(buffer, TIMESTRINGBUFFERSIZE, TIMEFORMAT, timeLocal);
    if(bytesWritten <= 0)
    {
        fprintf(stderr, "Konnte Zeit %ld nicht in string wandeln!\n", time);
        return 0;
    }
    return 1;
}
