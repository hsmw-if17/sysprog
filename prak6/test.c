#include <stdio.h>
#include <stdlib.h>

#include "mylib.h"

int main(int argc, char* argv[])
{
    if( argc != 2 )
    {
        printf("Nutzung:\n");
        printf("\t%s DATEINAME\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    /* isdir() testen */
    int result;
    if( (result = isdir(argv[1])) < 0)
    {
        perror("isdir");
        exit(EXIT_FAILURE);
    }

    if( result == 1 )
    {
        printf("Hurrah! Die Datei ist ein Verzeichnis!\n");
    }
    else
    {
        printf("Schade... kein Verzeichnis :(\n");
    }

    /* isSoftlink() testen */
    char* name;
    if( (name = isSoftlink(argv[1])) != NULL )
    {
        printf("Ist ein Softlink auf %s\n", name);
    }
    else /* mglw Fehler! */
    {
        printf("Ist kein Softlink.\n");
    }

    return EXIT_SUCCESS;
}
