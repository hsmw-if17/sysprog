#ifndef LIB_MYLIB_H
#define LIB_MYLIB_H

/** Prüfe ob Datei ein Verzeichnis ist.
 * \param filename Pfad zu der zu prüfenden Datei
 * \returns 1 (wahr) falls ein Verzeichnis, -1 falls Fehler, sonst 0
 */
int isdir(char* filename);

/** Prüfe ob Datei ein symlink ist.
 * \param filename Pfad zur zu prüfenden Datei
 * \returns Name der Datei auf die der symlink zeigt, sonst NULL
 */
char* isSoftlink(char* filename);

#endif /* LIB_MYLIB_H */
