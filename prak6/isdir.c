#include <unistd.h> /* stat() */
#include <sys/stat.h> /* struct stat */

#include "mylib.h" /* funktionsprototyp */

int isdir(char* filename)
{
    struct stat eigenschaften;

    /* Eigenschaften abrufen */
    if( stat(filename, &eigenschaften) < 0 )
    {
        /* im Fehlerfall -1 */
        /* errno von stat gesetzt, kann im Hauptprogramm abgerufen werden */
        return -1;
    }

    /* falls Verzeichnis... */
    if( (eigenschaften.st_mode & S_IFMT) == S_IFDIR )
    {
        /* gibt WAHR zurück */
        return 1;
    }
    /*sonst, gib FALSCH zurück*/
    return 0;
}
