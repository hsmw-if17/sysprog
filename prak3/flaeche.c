/*fläche.h hier inkludieren, weil da der Prototyp steht!
 *wenn wir den ändern müssen wir auch die Funktion hier ändern
 *könnten wir aber ohne den Prototyp vergessen, weil dann keine Warnung / Error!
 */
#include "flaeche.h"

#include <math.h> /*asin()*/

#include <stdio.h> /*printf()*/

/*static = kann nur in dieser Datei verwendet werden*/
static double Pi()
{
    /*berechne PI
     *sin(90°) = 1 => asin(1) = 90°
     *90° == pi/2 rad
     *2*pi/2 = pi
     *math.h funktionen rechnen standardmäßig mit Radians
     */
    return 2 * asin(1);
}

/* Definition der Funktion kreisFlaeche() */
double kreisFlaeche(double radius)
{
    /*static = wert bleibt zwischen aufrufen verfügbar
     *wird NICHT bei jedem aufruf auf 0 gesetzt! nur beim ersten mal!
     */
    static int numAufrufe = 0;
    ++numAufrufe; /*inkrementiert bei jedem aufruf um 1*/

    /*warnung nach zusatz 6*/
    if(numAufrufe >= 5)
    {
        printf("Warnung! kreisFlaeche() oft aufgerufen!\n");
    }

    /*berechne Pi und dann kreis Fläche*/
    return Pi()*radius*radius;
}


