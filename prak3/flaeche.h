#ifndef FLAECHE_H
#define FLAECHE_H

/*prototyp für berechnung von Pi
 *auskommentiert wegen Zusatz 5
 *double Pi();
 */

/*Funktionsprototyp für Kreisflächenberechnung*/
double kreisFlaeche(double radius);

#endif /*FLAECHE_H*/
