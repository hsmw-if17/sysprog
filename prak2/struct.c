#include <stdio.h>
#include <stdlib.h>

/*Struktur definieren*/
struct test {
    char c;
    int x;
};

/*Union definieren*/
union test2 {
    struct test s;
    /* das ist KEIN dynamisch großes array!
     * sizeof(struct test) ist zur compile zeit bekannt
     */
    unsigned char c[sizeof(struct test)];
};


int main()
{
    /*struktur anlegen und initialisieren*/
    struct test myVar;
    myVar.c = 'z';
    myVar.x = 1000000;

    /*union anlegen und initialisieren*/
    union test2 myVar2;
    myVar2.s = myVar;

    /*gib alle bits der Union aus*/
    for(int i = 0; i < sizeof(struct test); i++)
        printf("%i: %0#4x\n", i, myVar2.c[i]);

    return EXIT_SUCCESS;
}
