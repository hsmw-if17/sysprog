#include <stdio.h>
#include <stdlib.h>

int main()
{
    /*lu = long unsigned, nötig weil sizeof() size_t liefert.
     *mir ist keine Variante bekannt dies in einem Loop oder sonst irgendwie kürzer zu fassen
     */
    printf("Der Typ char belegt %lu Bytes\n", sizeof(char));
    printf("Der Typ short belegt %lu Bytes\n", sizeof(short));
    printf("Der Typ int belegt %lu Bytes\n", sizeof(int));
    printf("Der Typ long belegt %lu Bytes\n", sizeof(long));
    printf("Der Typ float belegt %lu Bytes\n", sizeof(float));
    printf("Der Typ double belegt %lu Bytes\n", sizeof(double));
    return EXIT_SUCCESS;
}
