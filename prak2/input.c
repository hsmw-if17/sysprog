#include <stdio.h>
#include <stdlib.h>

int main()
{
    int err;
    char input;

    do {
        /*eingabe abfrage*/
        printf("? ");
        err = scanf(" %c", &input); /*lies einzelnen Character*/
        while(getchar()!='\n')
            ;     /*restliche chars und newline verwerfen*/

        /*c = character*/
        /*03u = vornullen, 3 stellen, unsigned int*/
        /*%#4X = vorzeichen 0x, 4 stellen, Hexadezimal ABCDEF (x = abcdef)*/
        printf("%c %03u %#4X\n", input, input, input);
    } while( input != 'Q'); //Q beendet


    return EXIT_SUCCESS;
}
