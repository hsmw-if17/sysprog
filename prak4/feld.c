#include <stdio.h>
#include <stdlib.h>

#include <time.h>

/*funktionsprototypen*/
void zeigeFeld(int* feld, int anzahl);
void swap(int **a, int **b);

int main(int argc, char* argv[])
{
    /*fehler falls nicht genug argumente*/
    if(argc < 2)
    {
        fprintf(stderr, "Usage: feld N\n");
        return EXIT_FAILURE;
    }

    /*initialisiere RNG*/
    srand( time(NULL) );

    /* argv[0] = programmname
     * argv[1] = erster parameter
     * !! atoi gibt '0' wieder falls string ungültig, keine fehler!!
     */
    int anzahl = atoi( argv[1] );
    printf("anzahl = %i\n", anzahl);

    /*feld anlegen*/
    int* feld = malloc(anzahl * sizeof(int));

    /*feld mit zufallszahlen füllen ; dazu pointer arithmetic*/
    /*(kleiner 100 da bessere lesbarkeit beim sortieren)*/
    int *ptr = feld;
    while(ptr < feld+anzahl)
    {
        /*daten am pointer = zufällige zahl < 100
         *danach pointer ++
         */
        *(ptr++) = rand() % 100;
    }

    /*zeige das Feld an*/
    zeigeFeld(feld, anzahl);

    /*lege ein Feld von int* an*/
    int** sortierFeld = malloc(sizeof(int*) * anzahl);

    /*initialisiere mit pointern zu den ints in feld*/
    for(int i = 0; i < anzahl; i++)
    {
        sortierFeld[i] = &(feld[i]); /*sortierfeld[i] = addresse vom i-ten element*/
    }

    /*sortiere das feld*/
    for( int i = 0; i < anzahl; i++)
        for( int j = 0; j < anzahl; j++)
            if( *sortierFeld[i] < *sortierFeld[j] )
                swap( &sortierFeld[i], &sortierFeld[j] );

    /*gib das sortierte Feld aus
     *funktion zeigeFeld() geht hier nicht weil sortierFeld vom Type int**
     *funktion aber typ int* erwartet
     */
    printf("sortiertes Feld: \n");
    for(int i = 0; i < anzahl; i++)
    {
        printf("%i\n", *sortierFeld[i]);
    }

    free(feld);
    free(sortierFeld);
    return EXIT_SUCCESS;
}


void zeigeFeld(int* feld, int anzahl)
{
    printf("Feld: \n");
    int *end = feld+anzahl;
    while( feld < end)
    {
        printf("%i\n", *(feld++));
    }
    printf("---------------\n");
}

/*erhalte eine Referenz (pointer) zu einem int* */
void swap(int **a, int **b)
{
    /*tausche die inhalte der pointer(!)
     *tauscht die Adressen die in int* a, bzw int* b stehen!
     *tauscht NICHT die ints selber
     */
    int *tmp = *a;
    *a = *b;
    *b = tmp;
}
