#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(char** a, char** b);

int main(int argc, char** argv)
{
    /*anzahl an argumenten nach programmname */
    int anzahl = argc-1;
    printf("Argument Anzahl: %i\n", anzahl);

    /* sortierfeld == kopie von argv */

    /* char* sortierFeld[anzahl]; ist illegal, da dynamisch-große Arrays nicht unterstützt werden */

    /*feld von pointern zu zeichenketten*/
    char** sortierFeld = malloc( anzahl * sizeof(char*) );

    /*initialisiere mit adressen der argumente*/
    for(int i = 0; i < anzahl; i++)
        sortierFeld[i] = argv[i+1];

    /*sortiere das Feld*/
    for(int i = 0; i < anzahl; i++)
        for(int j = 0; j < anzahl; j++)
            if( strcmp( sortierFeld[i], sortierFeld[j] ) < 0 )
                swap( &sortierFeld[i], &sortierFeld[j] );

    /*git das sortierte Array aus*/
    printf("sortiert: \n");
    for(int i = 0; i < anzahl; i++)
        printf("%s\n", sortierFeld[i]);

    /* sortiertes Feld wieder löschen */
    free(sortierFeld);
    return EXIT_SUCCESS;
}

/* hier übergabe einer REFERENZ (pointer) eines char*! (daher char**)
NICHT übergabe eines char* arrays! */
void swap(char** a, char** b)
{
    /*tausche die string pointer
     *da eine referenz ebenfalls ein pointer ist
     *müssen wir dereferenzieren...*/
    char* tmp = *a;
    *a = *b;
    *b = tmp;
}
