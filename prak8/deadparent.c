#include <stdlib.h>
#include <stdio.h> /* printf, perror */

#include <unistd.h> /* fork, getppid, sleep */
#include <sys/types.h> /* pid_t */

int main(int argc, char** argv)
{
    pid_t cpid, ppid;

    cpid = fork();

    if( cpid > 0 ) /* parent */
    {
        sleep(2);
    }
    else if (cpid == 0) /* child */
    {
        ppid = getppid();
        printf("PPID=%d\n", ppid);
        sleep(4);
        ppid = getppid();
        printf("PPID=%d\n", ppid);
    }
    else
    {
        perror("fork");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
