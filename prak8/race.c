#include <stdlib.h>
#include <stdio.h> /* printf, perror */

#include <unistd.h> /* fork, getpid, getppid */
#include <sys/types.h> /* pid_t */
#include <sys/wait.h> /* waitpid */

int main(int argc, char** argv)
{
    pid_t pid, ppid, cpid, wpid;
    int status;

    cpid = fork();

    if(cpid < 0)
    {
        perror("fork");
        return EXIT_FAILURE;
    }

    /* beide Prozesse sollen dasselbe tun,
     also keine Unterscheidung hier*/

    pid = getpid();
    ppid = getppid();

    printf("PID=%d, PPID=%d, CPID=%d\n", pid, ppid, cpid);

    for(long i = 0; i < 1000000000; i++ )
    {
        /* BEGIN ZUSATZ: */

        /* END ZUSATZ */
    }

    if( cpid > 0) /* NUR Elternprozess */
    {
        /* WNOHANG => sofort zurückkehren, nicht warten. */
        wpid = waitpid(cpid, &status, WNOHANG );
        if( wpid > 0 ) /* Kind tot */
        {
            printf("Der Kindprozess hat das Rennen gewonnen!\n");
        }
        else if( wpid == 0 ) /* Kind lebt noch */
        {
            printf("Eltern haben eben längere Beine...\n");
        }
        else /* Fehler */
        {
            perror("waitpid");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
