#include <stdlib.h>
#include <stdio.h> /* printf, perror */

#include <unistd.h> /* fork, getpid, getppid */
#include <sys/types.h> /* pid_t */
#include <sys/wait.h> /* waitpid */

#include <string.h>

int main(int argc, char** argv)
{
    if( argc < 2 )
    {
        printf("Nutzung:\n");
        printf("\t%s PROGRAMM [ARGUMENT]...\n", argv[0]);
        return EXIT_FAILURE;
    }

    pid_t cpid, wpid;

    /* Argumentvektor für Kind anlegen */
    /* Wichtig: Programmname AUCH in das argv kopieren! */
    /* argv[0] darf NIEMALS null sein! */
    char** child_argv = malloc( (argc) * sizeof(char*) );

    for(int i = 1; i < argc; i++)
    {
        /* Parameter hineinkopieren */
        int len = strlen(argv[i])+1;
        child_argv[i-1] = malloc( len * sizeof(char) );
        strncpy( child_argv[i-1], argv[i], len );
    }

    /* Wichtig: Letztes Element der Liste MUSS null sein! */
    child_argv[argc-1] = NULL;

    cpid = fork();

    if(cpid > 0) /* parent */
    {
        int status;
        wpid = wait(&status);
        printf("Kind %d hat mit %d beendet.\n", wpid, status);
    }
    else if(cpid == 0) /* child */
    {
        if( execvp(argv[1], child_argv) < 0 )
        {
            perror("Execvp");
            return EXIT_FAILURE;
        }
    }
    else
    {
        perror("fork");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
