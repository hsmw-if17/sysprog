/* includes und Konstanten */
#include "common.h"

#include <errno.h>

//timeout includes
#include <time.h>

//signalhandler
#include <signal.h>


void delete_shm()
{
	if( shm_unlink(SHMEM_NAME) < 0)
	{
		perror("unlink shmem");
	}
}

void delete_getsem()
{
	if(sem_unlink(GETSEM_NAME) < 0)
	{
		perror("unlink getsem");
	}
}

void delete_putsem()
{
	if(sem_unlink(PUTSEM_NAME) < 0)
	{
		perror("unlink putsem");
	}
}

void signalhandler(int signal)
{
    exit(EXIT_FAILURE); /* beende mit Success (da gewollt) */
}

int main(int argc, char** argv)
{
    char* filepath = "ziel.dat";
    int fd;

    int fd_shmem;
    char* shmem;

    sem_t* getsem;
    sem_t* putsem;

    /* installiere Signalhandler für SIGINT */
    struct sigaction handler;
    handler.sa_handler = signalhandler;
    handler.sa_flags = 0;
    sigemptyset(&handler.sa_mask);

    if( sigaction(SIGINT, &handler, NULL) < 0 )
    {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }


    printf("oeffne Zieldatei!\n");

    /* versuche, datei zum lesen zu öffnen */
	/* überschreibe falls vorhanden */
    if( (fd = open(filepath, O_WRONLY | O_CREAT | O_TRUNC, 0644)) < 0 )
    {
        perror("datei oeffnen");
        exit(EXIT_FAILURE);
    }

    printf("erzeuge Ringpuffer!\n");

    /* ringpuffer erzeugen */
	/* überschreibe falls vorhanden */
    fd_shmem = shm_open(SHMEM_NAME, O_RDWR | O_CREAT | O_TRUNC, SHMEM_MODE);
    if(fd_shmem < 0)
    {
        perror("sharedmem erzeugen");
        exit(EXIT_FAILURE);
    }

	/* exit handler zum löschen der shared memory */
	atexit(delete_shm);

	/* mache puffer geeignet groß */
	if( ftruncate(fd_shmem, SHMEM_SIZE) < 0)
	{
		perror("resize shared memory file");
		exit(EXIT_FAILURE);
	}

    shmem = mmap(NULL, SHMEM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd_shmem, 0);
    if(shmem == MAP_FAILED)
    {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    printf("erzeuge semaphoren\n");

    /* semaphor erzeugen */
    putsem = sem_open(PUTSEM_NAME, O_CREAT|O_EXCL, 
                            SEMAPHOR_MODE, PUTSEM_INIT);
    if( putsem == SEM_FAILED )
    {
		if( errno == EEXIST )
		{
			putsem = sem_open(PUTSEM_NAME, 0);
		}
		else
		{
        	perror("put semaphor erzeugen");
        	exit(EXIT_FAILURE);
		}
    }

	//exithandler um semaphor zu löschen
	atexit(delete_putsem);

    getsem = sem_open(GETSEM_NAME, O_CREAT|O_EXCL, 
                            SEMAPHOR_MODE, GETSEM_INIT);
    if( getsem == SEM_FAILED )
    {
		if( errno == EEXIST )
		{
			getsem = sem_open(GETSEM_NAME, 0);
		}
		else
		{
        	perror("get semaphor erzeugen");
        	exit(EXIT_FAILURE);
		}
    }

	//exithandler löscht semaphor
	atexit(delete_getsem);

    
    /* HIER BYTEWEISE AUS RINGPUFFER LESEN */
	int bytesWritten = 0;
	char* position = shmem;	
	struct timespec timeout;

	do 
	{
		// 5 Sekunden timeout
		clock_gettime(CLOCK_REALTIME, &timeout);
		timeout.tv_sec += 5;

        printf("Warte auf Daten...\n");

		//warten, bis wir lesen können
		if( sem_timedwait(getsem, &timeout) < 0 )
		{
			if( errno == ETIMEDOUT )
			{
				printf("Erhalte keine Daten, beende\n");
				exit(EXIT_SUCCESS);
			}
			else
			{
				perror("wait getsem");
				exit(EXIT_FAILURE);
			}
		}
		
        printf("...schreibe erhaltene Daten\n");

		//schreibe 1 byte von position in fd
		bytesWritten = write(fd, position, 1);

		//falls schreibfehler
		if(bytesWritten < 0)
		{
			perror("write");
			exit(EXIT_FAILURE);
		}

		//position inkrementieren, überlauf beachten
		position += bytesWritten;
		if(position >= (shmem+SHMEM_SIZE)) position = shmem;
		
		//signalisieren, dass ein byte geschrieben werden kann
		if( sem_post(putsem) < 0 )
		{
			perror("post putsem");
			exit(EXIT_FAILURE);
		}

	} while( bytesWritten > 0 );

    printf("fertig, schließe ressourcen\n");

    /*alles ordnungsgerecht schließen*/
    munmap(shmem, SHMEM_SIZE);
    close(fd_shmem);
    close(fd);
    return EXIT_SUCCESS;
}
