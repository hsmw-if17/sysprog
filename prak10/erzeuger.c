/* includes und Konstanten */
#include "common.h"

int main(int argc, char** argv)
{
    if( argc != 2 )
    {
        printf("Benutzung:\n");
        printf("\t%s DATEINAME\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char* filepath = argv[1];
    int fd;

    int fd_shmem;
    char* shmem;

    sem_t* putsem;
    sem_t* getsem;

    printf("oeffne Quelldatei\n");    

    /* versuche, datei zum lesen zu öffnen */
    if( (fd = open(filepath, O_RDONLY)) < 0 )
    {
        perror("datei oeffnen");
        exit(EXIT_FAILURE);
    }

    printf("oeffne shared memory\n");

    /* ringpuffer öffnen (vom server erzeugt) */
    fd_shmem = shm_open(SHMEM_NAME, O_RDWR, SHMEM_MODE);
    if(fd_shmem < 0)
    {
        perror("sharedmem oeffnen");
        exit(EXIT_FAILURE);
    }

    shmem = mmap(NULL, SHMEM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd_shmem, 0);
    if(shmem == MAP_FAILED)
    {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    printf("oeffne semaphoren\n");

    /* semaphor öffnen (von server erzeugt) */
    putsem = sem_open(PUTSEM_NAME, 0);
    if( putsem == SEM_FAILED )
    {
        perror("put semaphor oeffnen");
        exit(EXIT_FAILURE);
    }

    getsem = sem_open(GETSEM_NAME, 0);
    if( getsem == SEM_FAILED )
    {
        perror("get semaphor oeffnen");
        exit(EXIT_FAILURE);
    }



    /* HIER BYTEWEISE IN DEN SHMEM KOPIEREN */
	int bytesRead = 0;
	char* position = shmem;

	do 
	{
        printf("warte auf schreibrecht...\n");

		//warte darauf, dass wir schreiben können
		if( sem_wait(putsem) < 0)
		{
			perror("wait putsem");
			exit(EXIT_FAILURE);
		}

        printf("...schreibe daten\n");

		//lies 1 byte von fd nach "position" (im shared mem)
		bytesRead = read(fd, position, 1);
		
		//falls bytesRead == 0, Ende OHEN SEM_POST!!! (sonst extra nullbyte in ziel!)
        if( bytesRead > 0 )
        {
		    //position weiterrücken und überlauf beachten
		    position += bytesRead;		
		    if( position >= (shmem+SHMEM_SIZE)) position = shmem;
	
		    //signalisieren, dass gelesen werden kann
		    if( sem_post(getsem) < 0 )
		    {
			    perror("post getsem");
			    exit(EXIT_FAILURE);
		    }
        }
        else if(bytesRead < 0)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
	} while( bytesRead > 0 );

    printf("fertig, schliesse ressourcen\n");

    /*alles ordnungsgerecht schließen*/
    munmap(shmem, SHMEM_SIZE);
    close(fd_shmem);
    close(fd);
    return EXIT_SUCCESS;
}
