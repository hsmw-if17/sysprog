#ifndef PRAK10_COMMON_H
#define PRAK10_COMMON_H

/*STANDARD*/
#include <stdio.h>
#include <stdlib.h>

/*DATEIEN*/
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

/*IPC*/
#include <sys/mman.h>
#include <semaphore.h>

/*KONSTANTEN*/
#define SHMEM_NAME "/prak10shm"
#define SHMEM_MODE 0660
#define SHMEM_SIZE 1024

#define PUTSEM_NAME "/prak10putsem"
#define PUTSEM_INIT SHMEM_SIZE

#define SEMAPHOR_MODE 0660

#define GETSEM_NAME "/prak10getsem"
#define GETSEM_INIT 0

#endif //PRAK10_COMMON_H
