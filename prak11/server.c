/* signal handler */
#include <signal.h>

#include "common.h"

int socketfd;

void close_socket(void)
{
    close(socketfd);
}

void delete_socket(void)
{
    unlink(SOCKET_PATH);    
}

void signalhandler(int signal)
{
    /* ruft dann automatisch installierte exithandler */
    exit(EXIT_SUCCESS);
}

void send_response(int type, int socket)
{
    static struct server_response response;

    response.type = type;
    response.err = errno;
    handle_error(
        write(socket, &response, sizeof(struct server_response)),
        "write response"
    );
}

int main(int argc, char** argv)
{
    /* variablen */
    int accept_socket;
    struct sockaddr_un addr;
    struct sigaction new_action;

    int bytesRead, bytesWritten;
    struct file_information finfo;
    int filefd;
    char fdata[BLOCK_SIZE];
    int msgt;

    /* signalhandler installieren  */
    sigemptyset(&new_action.sa_mask);
    new_action.sa_handler = signalhandler;
    new_action.sa_flags = 0;

    handle_error(
        sigaction(SIGINT, &new_action, NULL), "sigaction"
    );

    /* socket filedescriptor holen */
    socketfd = socket(AF_LOCAL, SOCK_STREAM, 0);
    handle_error(socketfd, "socket oeffnen");

    /* exit handler 1 */
    atexit(close_socket);

    /* scoket addresse vorbereiten */
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = PF_LOCAL;
    strncpy( addr.sun_path, SOCKET_PATH, sizeof(addr.sun_path)-1 );

    /* socket addresse zuweisen */
    handle_error( 
        bind(socketfd, (struct sockaddr*)&addr, sizeof(addr)), "bind socket"
    );

    /* exithandler 2 */
    atexit(delete_socket);

    /* höre bis zu 5 clients zu */
    handle_error(
        listen(socketfd, 5), "listen socket"
    );

    while(1)
    {
        /* unser socket, client address struct, struct größe
        (client addresse egal, deshalb null) */
        accept_socket = accept( socketfd, NULL, NULL );
        handle_error(accept_socket, "accept");

        /* falls nächste Nachricht NICHT Dateiinfo ist, nächster! */
        bytesRead = read(accept_socket, &msgt, sizeof(int));
        handle_error(bytesRead, "read msg type");
        if(msgt != MSGT_INFO)
        {
            close(accept_socket);
            continue;    
        }

        /* Empfange Dateiinformationen */
        bytesRead = read(accept_socket, &finfo, sizeof(struct file_information));
        handle_error( bytesRead, "read file info" );

        /* oeffne Datei */
        filefd = open( finfo.name, O_CREAT | O_EXCL | O_WRONLY, 0644 );
        
        /* falls Fehler, sende Fehler an Client */
        if( filefd < 0 )
        {
            send_response(MSGT_ERROR, accept_socket);
            close(accept_socket);
            continue;
        }

        /* versuche größe zu setzen, falls fehler, sende fehler an client */
        if( ftruncate(filefd, finfo.size) < 0 )
        {
            send_response(MSGT_ERROR, accept_socket);
            close(accept_socket);
            continue;
        }

        /* falls alles gut, sende success zu client */
        send_response(MSGT_SUCCESS, accept_socket);

        while(1)
        {
            bytesRead = read(accept_socket, &msgt, sizeof(int));
            handle_error(bytesRead, "read msg type");
            if( msgt == MSGT_BYE )
            {
                break; /* loop beenden, keine weiteren daten! */    
            }
            else if( msgt == MSGT_BLOCK )
            {
                bytesRead = read(accept_socket, &fdata, BLOCK_SIZE);
                handle_error(bytesRead, "read file data");
            
                /* versuche daten in datei zu schreiben */
                bytesWritten = write(filefd, &fdata, bytesRead);
                if( bytesWritten < 0 )
                {
                    send_response( MSGT_ERROR, accept_socket );
                    break; /* fehler, loop beenden! */
                }
                send_response( MSGT_SUCCESS, accept_socket );
            }
        }

        /* fertig, socket schließen */
        close(filefd);
        close(accept_socket);
    }

    return EXIT_SUCCESS;    
}
