#include <stdlib.h> /* getnenv */
#include <linux/limits.h> /* PATH_MAX */
#include <dirent.h> /* verzeichnisse lesen */
#include <string.h> /* strlen, strcpy, strcat */
#include <stdio.h> /* perror */

/* Hilffunktion um nächsten Eintrag zu lesen
List = "FIRST:SECOND:THIRD:..." */
static char* getNextInList(char* list)
{
    static long offset = 0;
    static char buffer[PATH_MAX];

    if( offset >= strlen(list) )
    {
        /* liste zu Ende */
        return NULL;
    }

    int i = 0;
    while( list[i+offset] != ':' && list[i+offset] != '\0' )
    {
        buffer[i] = list[i+offset];
        i++;
    }

    buffer[i] = '\0'; /* string terminieren! */
    ++i; /* doppelpunkt überspringen! */
    offset += i;

    return buffer;
}

char* GetFullPathName(char* name, char* ENVname)
{
    char* ENVval;
    char* path;
    static char fullpath[PATH_MAX];
    DIR* verzeichnis;
    struct dirent* eintrag;
    int found = 0;

    /* Environment Variable holen */
    ENVval = getenv(ENVname);
    if( ENVval == NULL )
    {
        return NULL;
    }

    /* über alle verzeichnisse iterieren */
    while( (path = getNextInList(ENVval)) != NULL )
    {
        /* verzeichnis öffnen */
        verzeichnis = opendir(path);
        if(verzeichnis == NULL)
        {
            fprintf(stderr, "Konnte Verzeichnis %s nicht oeffnen: ", path);
            perror("");
            continue; /*nicht programm abbrechen, sondern versuchen nächstes Verzeichnis zu öffnen*/
        }

        /* über alle einträge iterieren */
        while( (eintrag = readdir(verzeichnis)) != NULL )
        {
            if(strcmp(name, eintrag->d_name) == 0)
            {
                /* gefunden, verlasse eintrag-loop */
                found = 1;
                break;
            }
        }

        /* gefunden, verlasse verzeichnis loop */
        if(found == 1)
        {
            size_t len = strlen(path);

            /* problem falls größer als unser rückgabe array */
            if(len >= PATH_MAX-1)
            {
                return NULL;
            }

            /* len+1, weil wir das \0 auch kopieren wollen */
            strncpy(fullpath, path, len+1);

            /* "/" an Pfad anhängen, falls nicht auf "/" endet */
            if( fullpath[len-1] != '/' )
            {
                fullpath[len] = '/';
                fullpath[len+1] = '\0';
            }

            /* problem falls nicht genug platz */
            size_t len2 = strlen(name);
            if( len+len2 >= PATH_MAX-1 )
            {
                return NULL;
            }

            strncat(fullpath, name, len2+1);
            return fullpath;
        }
    }

    return NULL; /* sonst */
}
