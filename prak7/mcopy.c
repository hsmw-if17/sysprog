#include <stdlib.h> /* standardfuntionen */
#include <sys/stat.h> /* stat() und struct stat */
#include <unistd.h> /* dateifunktionen */
#include <fcntl.h>  /* mehr dateifunktionen */
#include <stdio.h> /* fprintf */
#include <sys/mman.h> /* mmap, munmap */
#include <string.h> /* memcpy (warum in string?) */

#define BUFFERSIZE 1024

void usage();

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        usage(); /* bei falscher parameteranzahl */
    }

    char* src_path = argv[1]; /* pfad quelle */
    char* dst_path = argv[2]; /* pfad ziel */

    int fd_src; /* filedeskriptor quelle */
    int fd_dst; /* filedeskriptor ziel */

    struct stat src_stat; /* dateieigenschaften der quelldatei */

    size_t length; /* Dateilänge in bytes */

    void* src_addr = NULL; /* mapped Adresse quelldatei */
    void* dst_addr = NULL; /* mapped Adresse zieldatei */

    /* öffne quelldatei zum lesen */
    fd_src = open( src_path, O_RDONLY );
    if( fd_src < 0)
    {
        /*error falls keine leserechte oder datei nicht existent*/
        perror("quelldatei oeffnen");
        exit(EXIT_FAILURE);
    }

    /* falls zieldatei existiert, frage nach überschreiben */
    if( access(dst_path, F_OK) == 0 )
    {
        char in;
        printf("Wollen sie die Datei Ueberschreiben? (j/n)");
        scanf(" %c", &in);
        while(getchar()!='\n')
            ;

        if( in != 'j')
        {
            exit(EXIT_FAILURE); /* hier eigenen return code? da abbruch, nicht fehlschlag */
        }
    }

    /*öffne zieldatei nur zum schreiben und lösche allen inhalt und erstelle falls nicht existiert*/
    /*MUSS READWRITE sein! Sonst gibt mmap() permission denied!*/
    fd_dst = open(dst_path, O_RDWR | O_CREAT);
    if(fd_dst < 0)
    {
        /*error falls datei nicht schreibbar*/
        perror("zieldatei oeffnen");
        exit(EXIT_FAILURE);
    }

    

    /*hole quelldatei eigenschaften*/
    if( fstat(fd_src, &src_stat) < 0 )
    {
        /*mögliche errors fangen*/
        perror("quelldatei stat");
        exit(EXIT_FAILURE);
    }

    /* ändere zieldatei rechte auf quelldatei rechte */
    if( fchmod(fd_dst, src_stat.st_mode) < 0)
    {
        /*mögliche errors fangen*/
        perror("zieldatei chmod");
        exit(EXIT_FAILURE);
    }

    /*es gab keine errors, also können wir jetzt kopieren*/

    /* länge der zu kopierenden Datei finden */
    length = src_stat.st_size;

    /*schneide Zieldatei auf länge der Quelldatei*/
	/*sonst memcpy BUS fehler*/
    if( ftruncate(fd_dst, length) < 0)
    {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    /* Dateien in den Speicher mappen */

    /* PROT_READ => wir wollen nur lesen
    MAP_PRIVATE => speicheränderungen nicht auf Datei zurückschreiben */
    src_addr = mmap(0, length, PROT_READ, MAP_PRIVATE, fd_src, 0);

    if(src_addr == MAP_FAILED)
    {
        perror("Quelldatei mappen");
        exit(EXIT_FAILURE);
    }

    /* PROT_WRITE => wir wollen schreiben
    MAP_SHARED => speicheränderungen zurück in Datei schreiben */
    dst_addr = mmap(0, length, PROT_WRITE, MAP_SHARED, fd_dst, 0);

    if(dst_addr == MAP_FAILED)
    {
        perror("Zieldatei mappen");
        exit(EXIT_FAILURE);
    }

    /* Speicher kopieren */
    memcpy( dst_addr, src_addr, length );

    /* Dateien aus dem Speicher nehmen */
    munmap(dst_addr, length);
    munmap(src_addr, length);

    /*dateien schließen, wir sind fertig*/
    close(fd_src);
    close(fd_dst);

    return EXIT_SUCCESS;
}

void usage()
{
    printf("Benutzung: \n");
    printf("\tcopy quelldatei zieldatei\n");
    exit(EXIT_FAILURE);
}
