#include "fpn.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        printf("Nutzung:\n");
        printf("\t%s DATEINAME ENVVAR\n", argv[0]);
        return EXIT_FAILURE;
    }

    char* pfad;

    pfad = GetFullPathName(argv[1], argv[2]);

    if(pfad == NULL)
    {
        printf("Fehler, konnte nicht gefunden werden!\n");
        return EXIT_FAILURE;
    }

    printf("Datei gefunden in: %s\n", pfad);

    return EXIT_SUCCESS;
}