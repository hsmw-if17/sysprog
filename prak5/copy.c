#include <stdlib.h> /* standardfuntionen */
#include <sys/stat.h> /* stat() und struct stat */
#include <unistd.h> /* dateifunktionen */
#include <fcntl.h>  /* mehr dateifunktionen */
#include <stdio.h> /* fprintf */

#define BUFFERSIZE 1024

void usage();

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        usage(); /* bei falscher parameteranzahl */
    }

    char* src_path = argv[1]; /* pfad quelle */
    char* dst_path = argv[2]; /* pfad ziel */

    int fd_src; /* filedeskriptor quelle */
    int fd_dst; /* filedeskriptor ziel */

    struct stat src_stat; /* dateieigenschaften der quelldatei */

    /* öffne quelldatei zum lesen */
    fd_src = open( src_path, O_RDONLY );
    if( fd_src < 0)
    {
        /*error falls keine leserechte oder datei nicht existent*/
        perror("quelldatei oeffnen");
        exit(EXIT_FAILURE);
    }

    /* falls zieldatei existiert, frage nach überschreiben */
    if( access(dst_path, F_OK) == 0 )
    {
        char in;
        printf("Wollen sie die Datei Ueberschreiben? (j/n)");
        scanf(" %c", &in);
        while(getchar()!='\n')
            ;

        if( in != 'j')
        {
            exit(EXIT_FAILURE); /* hier eigenen return code? da abbruch, nicht fehlschlag */
        }
    }

    /*öffne zieldatei nur zum schreiben und lösche allen inhalt und erstelle falls nicht existiert*/
    fd_dst = open(dst_path, O_WRONLY | O_TRUNC | O_CREAT);
    if(fd_dst < 0)
    {
        /*error falls datei nicht schreibbar*/
        perror("zieldatei oeffnen");
        exit(EXIT_FAILURE);
    }

    /*hole quelldatei eigenschaften*/
    if( fstat(fd_src, &src_stat) < 0 )
    {
        /*mögliche errors fangen*/
        perror("quelldatei stat");
        exit(EXIT_FAILURE);
    }

    /* ändere zieldatei rechte auf quelldatei rechte */
    if( fchmod(fd_dst, src_stat.st_mode) < 0)
    {
        /*mögliche errors fangen*/
        perror("zieldatei chmod");
        exit(EXIT_FAILURE);
    }

    /*es gab keine errors, also können wir jetzt kopieren*/

    int bytesRead, bytesWrote;
    char buf[BUFFERSIZE]; /* buffer zum kopieren anlegen */

    while( (bytesRead = read(fd_src, &buf, BUFFERSIZE)) > 0 )
    {
        /*nur so viele byte schreiben wie gelesen wurden*/
        bytesWrote = write(fd_dst, &buf, bytesRead);
        if( bytesWrote < 0)
        {
            perror("zieldatei schreiben");
            exit(EXIT_FAILURE);
        }
        else if( bytesWrote != bytesRead )
        {
            fprintf(stderr, "es wurden weniger byte geschrieben als gelesen!\n");
        }
    }

    /*falls lesefehler...*/
    if(bytesRead < 0)
    {
        perror("quelldatei lesen");
        exit(EXIT_FAILURE);
    }

    /*dateien schließen, wir sind fertig*/
    close(fd_src);
    close(fd_dst);

    return EXIT_SUCCESS;
}

void usage()
{
    printf("Benutzung: \n");
    printf("\tcopy quelldatei zieldatei\n");
    exit(EXIT_FAILURE);
}
