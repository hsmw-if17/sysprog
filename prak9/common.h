#ifndef PRAK9_COMMON_H
#define PRAK9_COMMON_H

/* Wir definieren die Nachrichtengröße als PATH_MAX,
weil wir Dateipfade übermitteln wollen. 
wir werden Datenblöcke derselben größe übermitteln */

#include <limits.h>

#define MSGSIZE PATH_MAX

/* block-size ist entweder 1024, oder MSGSIZE falls diese kleiner ist */
#define BLKSIZE (1024 < MSGSIZE) ? (1024) : (MSGSIZE)

/* name, länge, blockdaten, success, error, ende */
/* Type MUSS GRÖSSER ALS 0 sein */
enum { MSGT_NAM = 1, MSGT_LEN, MSGT_BLK, MSGT_SUC, MSGT_ERR, MSGT_BYE };

struct msgbuf {
    long type;
    char text[MSGSIZE];
};


#endif /* PRAK9_COMMON_H */
