#include <stdlib.h>

#include <sys/types.h> /* pid_t */
#include <stdio.h> /* printf(), perror() */
#include <unistd.h> /* fork(), getpid(), pause() */
#include <sys/wait.h> /* waitpid() */
#include <signal.h> /* kill() */

void signalhandler(int signal)
{
    /* hier kein code, signalhandler nur nötig,
     * weil pause() auf terminierung oder signalhandler
     * wartet. Nicht auf andere signale.
     */
    return;
}

int main(int argc, char** argv)
{
    pid_t pid, cpid;
    struct sigaction handler;
    struct sigaction oldSigAction;

    /* sigaction initialisieren */
    handler.sa_handler = signalhandler;
    handler.sa_flags = 0;

    /* leere Signalmaske (nichts außer SIGUSR1 blockieren) */
    sigemptyset(&handler.sa_mask);

    /* 
     * signalhandler installieren
     * VOR fork()! Wird vererbt! 
     */
    if(sigaction(SIGUSR1, &handler, &oldSigAction) < 0)
    {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }

    cpid = fork();  /* erzeuge Kindprozess */

    if( cpid > 0 )  /* falls Elternprozess */
    {
        sleep(1); /* siehe Praktikums-Tipp */

        pid = getpid(); /* eigene PID */

        for( int i = 0; i < 20; i++ )
        {
            printf("Elternprozess PID: %d\n", pid); /* printe zuerst */
            kill( cpid, SIGUSR1 ); /* signalisiere Kind */
            pause();    /* warte auf SIGUSR1 von Kindprozess */
        }
        
        int cstatus;
        waitpid(cpid, &cstatus, 0); //auf ende des kindes warten.
    } 
    else if( cpid == 0 ) /* falls Kindprozess */
    {
        pid_t ppid;

        pid = getpid();     /* eigene PID */
        ppid = getppid();   /* parent PID */

        for(int i = 0; i < 20; i++ )
        {
            pause(); /* warte auf SIGUSR1 von Elternprozess */
            printf("Kindprozess PID: %d\n", pid); /* printe als zweites */
            kill(ppid, SIGUSR1); /* signalisiere Parent */
        }
    }
    else /* falls Fehler */
    {
        perror("Fork");
        exit(EXIT_FAILURE);
    }
    

    return EXIT_SUCCESS;    
}
