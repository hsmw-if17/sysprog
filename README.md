# SYSTEMPROG.IF17
Hier findet ihr Lösungen für die Praktika in Systemprogrammierung.  
Praktika 1 bis 4 sind Einführung in C. Danach wird es interessant.

### HINWEIS
In Praktika 1 und 3 fehlt die Datei "kreis.c".  
Diese kann aufgrund des Urheberrechts hier nicht hochgeladen werden.  
Wenn ihr euch für diese Repo interessiert, dann wisst ihr wo ihr sie herbekommt.

### LIZENZ
Code ist unter der MIT Lizenz. (Siehe LICENSE Datei)  
Viel Spaß!
